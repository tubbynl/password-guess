package nl.security4teens.apps.password.controller;

import lombok.extern.slf4j.Slf4j;
import nl.security4teens.apps.password.PasswordGuessApplication;
import nl.security4teens.apps.password.Request;
import nl.security4teens.apps.password.domain.response.Ok;
import nl.security4teens.apps.password.domain.response.Response;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("password")
@Slf4j
public class PasswordController {

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public Map<String, Integer> listPasswords() {
        return PasswordGuessApplication.passwords;
    }

    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public void resetPasswords() {
        PasswordGuessApplication.guessedPasswords = new ArrayList<>();
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = "application/json")
    public List<String> statusGuesses() {
        return PasswordGuessApplication.guessedPasswords;
    }

    @RequestMapping(value = "/guess/", method =  RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Response guessPasswordPost(@RequestBody Request req) {
        Integer prevPosition = PasswordGuessApplication.passwords.get(req.getPassword());
        if(prevPosition != null && !PasswordGuessApplication.guessedPasswords.contains(req.getPassword())) {
            PasswordGuessApplication.guessedPasswords.add(req.getPassword());
        }
        return Ok.builder()
                .found((prevPosition != null?true:false))
                .password(req.getPassword())
                .previous((prevPosition != null)?prevPosition:-1)
                .build();
    }

    @RequestMapping(value = "/guess/{password}", method =  RequestMethod.GET, produces = "application/json")
    public Response guessPassword(@PathVariable("password") String password) {
        Integer prevPosition = PasswordGuessApplication.passwords.get(password);
        if(prevPosition != null && !PasswordGuessApplication.guessedPasswords.contains(password)) {
            PasswordGuessApplication.guessedPasswords.add(password);
        }
        return Ok.builder()
                .found((prevPosition != null?true:false))
                .password(password)
                .previous((prevPosition != null)?prevPosition:-1)
                .build();
    }
}
